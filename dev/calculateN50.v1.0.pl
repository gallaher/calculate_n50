#!/usr/bin/perl

use warnings;
use strict;

# filename: calculateN50.pl
# author: Sean D. Gallaher
# date: 01-NOV-2018
# version: v1.0	
# description: This is a perl script to take a fasta file
# and calculate N50/L50 statistics for both
# contigs and scaffolds


my $help = "\nThis is a perl script to take a fasta file\nand calculate N50\/L50 statistics for both\ncontigs and scaffolds.\n\nPlease supply a fasta file as an arguement.\n\n";

# get the fast file from user input
my $fasta = shift @ARGV;


# otherwise, print help
if (!defined $fasta) {
	die $help;
}


# open the fasta file
open (IN, "<", $fasta) 
	or die "Could not open $fasta: $!\n\n";


# create arrays for the contig and scaffold sizes
my @contigs;
my @scaffolds;

# create counters for contigs and scaffolds
my $contigCount = 0;
my $scaffoldCount = 0;

# create scalar for assembly size
my $total;

# This is so that strings of "N"s get 
# propperly grouped between contigs
my $lastNt = 'N';


# read the fasta file line by line
# and calculate the sizes of the 
# contigs and scaffolds 

while (my $line = <IN>) {
	chomp $line;
	if ($line =~ m/^>/) {
		$scaffoldCount++;
		$contigCount++;
		$lastNt = 'N';
	}
	else {
		my @lineArray = split (//, $line);
		foreach my $nt (@lineArray) {
			$total++;
			$scaffolds[$scaffoldCount]++;
			if ($nt =~ m/[ACGTRSWVYKMBDHacgtrswvykmbdh]/) {
				$contigs[$contigCount]++;
				$lastNt = $nt;
			} 
			elsif ($nt =~ m/[NXnx\.-]/) {
				if ($lastNt =~ m/[ACGTRSWVYKMBDHacgtrswvykmbdh]/) {
					$contigCount++;
					$lastNt = $nt;	
				}
			}
			else {
				print "Non-IUPAC character: $nt\n";
			}
		}
	}
}

# remove any zero-length contigs or scaffolds

my @noZeroContigs;
my @noZeroScaffolds;
my $noZeroContigCount;
my $noZeroScaffoldCount;

foreach my $contig (@contigs) {
	if ((defined $contig) && ($contig != 0)) {
		push (@noZeroContigs, $contig);
		$noZeroContigCount++;
	}
}
foreach my $scaffold (@scaffolds) {
	if ((defined $scaffold) && ($scaffold != 0))  {
		push (@noZeroScaffolds, $scaffold);
		$noZeroScaffoldCount++;
	}
}


# sort the contigs and scaffolds by size

my @sortedContigs = sort { $a <=> $b } @noZeroContigs;
my @sortedScaffolds = sort { $a <=> $b } @noZeroScaffolds;

# perform the N50 / L50 calculations
my $halfAssembly = $total / 2;

my $accumulatedContigs;
my $accumulatedScaffolds;

my $contigL50;
my $scaffoldL50;

print "total assembly = $total bp\n";

for (my $i = $noZeroContigCount - 1 ; $i > 0; $i--) {
	$accumulatedContigs += $sortedContigs[$i];
	$contigL50++;
	if ($accumulatedContigs > $halfAssembly) {
		my $contigN50 = $sortedContigs[$i];
		print "total contigs = $noZeroContigCount\ncontig N50 = $contigN50 bp\ncontig L50 = $contigL50\n";
		$i=0;
	}
}

for (my $j = $noZeroScaffoldCount - 1 ; $j > 0; $j--) {
	$accumulatedScaffolds += $sortedScaffolds[$j];
	$scaffoldL50++;
	if ($accumulatedScaffolds > $halfAssembly) {
		my $scaffoldN50 = $sortedScaffolds[$j];
		print "total scaffolds = $noZeroScaffoldCount\nscaffold N50 = $scaffoldN50 bp\nscaffold L50 = $scaffoldL50\n";
		$j=0;
	}
}




close (IN);


