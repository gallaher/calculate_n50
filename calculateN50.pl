#!/usr/bin/perl

use warnings;
use strict;

# filename: calculateN50.pl
# author: Sean D. Gallaher
# date: 02-NOV-2018
# version: v1.1	
# description: This is a perl script to take a fasta file
# and calculate N50/L50 statistics for both
# contigs and scaffolds


my $help = "\nThis is a perl script to calculate N50\/L50 statistics for both\ncontigs and scaffolds for a supplied genome assembly.\n\nPlease supply a fasta file as an arguement.\n\nOptions:\n-m\tminimum sized contigs/scaffolds to include\n\t(default = 100)\n\nSample usage:\ncalculateN50.pl -m 1000 yourAssembly.fasta\ncalculateN50.pl yourAssembly.fa\n\n";


# set the minimum threshold
my $min = 100;


# get user input
my $fasta;

my $userInputs = scalar(@ARGV);
if ($userInputs == 0) {
	die $help;
}
elsif ($userInputs == 1) {
	$fasta = shift(@ARGV);
}
else {
	for (my $i = 0; $i < $userInputs; $i++) {
		my $input = $ARGV[$i];
		if ($input =~ m/-m/) {
			$i++;
			$min = $ARGV[$i];
		}
		else {
			$fasta = $ARGV[$i]
		}
	}
}




# check that all inputs are correct
# print help message if not
if (!defined $fasta) {
	die $help;
}
elsif ($fasta !~ m/.+\.(fa|fasta)$/) {
	die $help;
}
elsif ($min !~ m/^[0-9]+$/) {
	die $help;
}


# report settings to sdtout
print "\nassembly = $fasta\nminimum cutoff = $min bp\n";


# open the fasta file
open (IN, "<", $fasta) 
	or die "Could not open $fasta: $!\n\n";


# create arrays for the contig and scaffold sizes
my @contigs;
my @scaffolds;

# create counters for contigs and scaffolds
my $contigCount = 0;
my $scaffoldCount = 0;

# create scalar for assembly size
my $assemblyTotal;

# This is so that strings of "N"s get 
# propperly grouped between contigs
my $lastNt = 'N';


# read the fasta file line by line
# and calculate the sizes of the 
# contigs and scaffolds 

while (my $line = <IN>) {
	chomp $line;
	if ($line =~ m/^>/) {
		$scaffoldCount++;
		$contigCount++;
		$lastNt = 'N';
	}
	else {
		my @lineArray = split (//, $line);
		foreach my $nt (@lineArray) {
			$assemblyTotal++;
			$scaffolds[$scaffoldCount]++;
			if ($nt =~ m/[ACGTRSWVYKMBDHacgtrswvykmbdh]/) {
				$contigs[$contigCount]++;
				$lastNt = $nt;
			} 
			elsif ($nt =~ m/[NXnx\.-]/) {
				if ($lastNt =~ m/[ACGTRSWVYKMBDHacgtrswvykmbdh]/) {
					$contigCount++;
					$lastNt = $nt;	
				}
			}
			else {
				print "error: Non-IUPAC character \"$nt\"\n";
			}
		}
	}
}

# remove any zero-length contigs or scaffolds

my @minSizeContigs;
my @minSizeScaffolds;
my $minSizeContigCount;
my $minSizeScaffoldCount;
my $minSizeContigTotal;
my $minSizeScaffoldTotal;



foreach my $contig (@contigs) {
	if ((defined $contig) && ($contig >= $min)) {
		push (@minSizeContigs, $contig);
		$minSizeContigCount++;
		$minSizeContigTotal += $contig;
	}
}
foreach my $scaffold (@scaffolds) {
	if ((defined $scaffold) && ($scaffold >= $min))  {
		push (@minSizeScaffolds, $scaffold);
		$minSizeScaffoldCount++;
		$minSizeScaffoldTotal += $scaffold;
	}
}


# sort the contigs and scaffolds by size

my @sortedContigs = sort { $a <=> $b } @minSizeContigs;
my @sortedScaffolds = sort { $a <=> $b } @minSizeScaffolds;

# perform the N50 / L50 calculations
my $contigHalfAssembly = $minSizeContigTotal / 2;
my $scaffoldHalfAssembly = $minSizeScaffoldTotal / 2;

my $accumulatedContigs;
my $accumulatedScaffolds;

my $contigL50;
my $scaffoldL50;

print "\ntotal assembly = $assemblyTotal bp\n";

for (my $i = $minSizeContigCount - 1 ; $i > 0; $i--) {
	$accumulatedContigs += $sortedContigs[$i];
	$contigL50++;
	if ($accumulatedContigs > $contigHalfAssembly) {
		my $contigN50 = $sortedContigs[$i];
		print "\ntotal contigs = $minSizeContigCount\ncontig assembly size = $minSizeContigTotal bp\ncontig N50 = $contigN50 bp\ncontig L50 = $contigL50\n";
		$i=0;
	}
}

for (my $j = $minSizeScaffoldCount - 1 ; $j > 0; $j--) {
	$accumulatedScaffolds += $sortedScaffolds[$j];
	$scaffoldL50++;
	if ($accumulatedScaffolds > $scaffoldHalfAssembly) {
		my $scaffoldN50 = $sortedScaffolds[$j];
		print "\ntotal scaffolds = $minSizeScaffoldCount\nscaffold assembly size = $minSizeScaffoldTotal bp\nscaffold N50 = $scaffoldN50 bp\nscaffold L50 = $scaffoldL50\n\n";
		$j=0;
	}
}




close (IN);


