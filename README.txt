######################################
#####  calculateN50  README.txt  #####
######################################

Repository:
https://bitbucket.org/gallaher/calculateN50

Author: 
Sean Gallaher

Version:
v1.1

Date:
02-NOV-2018

This repository contains a perl script to calculate 
N50 / L50 statistics for both contigs and scaffolds 
for a supplied genome assembly.

For a description of N50 / L50, see:
https://en.wikipedia.org/wiki/N50,_L50,_and_related_statistics

The script takes an assembly in fasta format as input. 
The file name must end in ".fasta" or ".fa".

By default, contigs and scaffolds smaller than 100 bp
will be ignored. You can change that setting with the
-m flag. 

Sample usage:
calculateN50.pl -m 1000 yourAssembly.fasta
calculateN50.pl yourAssembly.fa

Please send comments, questions, or trouble-shooting
requests to the author, Sean Gallaher, at

gallaher@chem.ucla.edu

This software is made available for free under the 
GNU GENERAL PUBLIC LICENSE. A copy of the license 
should be included with the repository.
